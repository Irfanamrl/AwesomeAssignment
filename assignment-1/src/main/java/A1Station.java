import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TrainCar terakhir = null;
        String banyakKucing = sc.nextLine();
        int totalKucing = 0;
        int a = Integer.parseInt(banyakKucing);
        for (int i = 0; i < a; i++) {
            String[] masukan = sc.nextLine().split(",");
            Double berat = Double.parseDouble(masukan[1]);
            Double panjang = Double.parseDouble(masukan[2]);
            WildCat isi = new WildCat(masukan[0], berat, panjang);
            totalKucing++;
            if (terakhir == null){
                terakhir = new TrainCar(isi);
            }
            else{
                terakhir = new TrainCar(isi, terakhir);
            }
            double averageMassIndex = terakhir.computeTotalMassIndex()/totalKucing;
            if (terakhir.computeTotalWeight() > THRESHOLD){
                String output;
                System.out.println("The train departs to Javari Park");
                System.out.print("[LOCO]<--"); terakhir.printCar();
                System.out.println("\nAverage mass index of all cats: " + averageMassIndex);
                if (averageMassIndex >= 30){
                    output = "obese";
                }
                else if(averageMassIndex >= 25){
                    output = "overweight";
                }
                else if(averageMassIndex >= 18.5){
                    output = "normal";
                }
                else{
                    output = "underweight";
                }
                System.out.println("In average, the cats in the train are "+output);
                terakhir = null;
                totalKucing = 0;
                isi = null;
            }
            else if ( i == a-1) {
                String output;
                System.out.println("The train departs to Javari Park");
                System.out.print("[LOCO]<--"); terakhir.printCar();
                System.out.println("\nAverage mass index of all cats: " + averageMassIndex);
                if (averageMassIndex >= 30){
                    output = "obese";
                }
                else if(averageMassIndex >= 25){
                    output = "overweight";
                }
                else if(averageMassIndex >= 18.5){
                    output = "normal";
                }
                else{
                    output = "underweight";
                }
                System.out.println("In average, the cats in the train are "+output);
            }
        }






    }
}
