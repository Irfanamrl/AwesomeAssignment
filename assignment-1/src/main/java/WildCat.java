public class WildCat{
    String name;
    double weight; //in kg
    double length; //in cm

    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    public double computeMassIndex() {
        double bmi = this.weight * 10000 / (this.length * this.length);
        return bmi;
    }
    
}