import javax.swing.*;

/**
 * Class that represent Card of the game.
 */

public class JavariCards {
    private int id;
    private boolean matched = false;
    private JButton button = new JButton();
    private ImageIcon imgIcon;
    private ImageIcon imgPressed;

    public JavariCards(int id, ImageIcon imgIcon, ImageIcon imgPressed) {
        this.id = id;
        this.imgIcon = imgIcon;
        this.imgPressed = imgPressed;
        this.button.setIcon(imgIcon);
    }

    public int getID() {
        return this.id;
    }

    public boolean getMatched() {
        return this.matched;
    }

    public void setMatched(boolean matched) {
        this.matched = matched;
    }

    public JButton getButton() {
        return button;
    }

    public void press() {
        button.setIcon(imgPressed);
    }

    public void release() {
        button.setIcon(imgIcon);
    }
}