import java.awt.*;
import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;

/*
board game buatan irfan
*/

public class Board extends JFrame {
    private final int pairs = 18;
    private ArrayList<JavariCards> cards;
    private String path;
    private JavariCards kartuPertama;
    private JavariCards kartuKedua;
    private Timer timer;
    private Timer openTime;
    private Timer waitTime;
    private JFrame frameUtama;
    private JLabel tryLabel;
    private JLabel resetLabel;
    private JPanel panelAtas;
    private JPanel panelBawah;
    private JButton exitButton;
    private JButton resetButton;
    private int reset = 0;
    private int tries = 0;

    public Board() {
        ArrayList<JavariCards> listKartu = new ArrayList<>();
        ArrayList<Integer> nilaiKartu = new ArrayList<>();

        for (int i = 0; i < pairs; i++) {
            nilaiKartu.add(i);
            nilaiKartu.add(i);
        }
        Collections.shuffle(nilaiKartu);

        for (int value : nilaiKartu) {
            String[] computers = { "cable", "charger-2", "computer", "chip-1", "game-console", "game-console-3",
                    "game-console-7", "game-controller-2", "game-controller-4", "game-controller-8",
                    "headphones", "ipod-1", "joystick-1", "mouse-4", "photo-camera", "smartwatch",
                    "television-3", "typewriter" };
            path = "D:\\DDP2\\Assignment\\assignment-4\\src\\main\\img\\";
            JavariCards card = new JavariCards(value, resize(new ImageIcon((path + "cooler.png"))),
                    resize(new ImageIcon(path + computers[value] + ".png")));
            card.getButton().addActionListener(actionPerformed -> balikKartu(card));
            listKartu.add(card);
        }
        this.cards = listKartu;
        // membuat penghitung
        timer = new Timer(700, actionPerformed -> checkCards());
        timer.setRepeats(false);
        createPanel();
    }

    /**
     * Method untuk menciptakan panel
     */

    public void createPanel() {
        frameUtama = new JFrame("Match Pair Memory Game (Computer Edition)");
        frameUtama.setLayout(new BorderLayout());
        panelAtas = new JPanel(new GridLayout(6, 6));
        for (JavariCards card : cards) {
            panelAtas.add(card.getButton());
        }
        JPanel panelBawah = new JPanel(new GridLayout(2, 2));
        JButton resetButton = new JButton("Reset Game");
        JButton quitButton = new JButton("Quit");
        panelBawah.add(resetButton);
        panelBawah.add(quitButton);
        resetLabel = new JLabel("Number of Reset: " + reset);
        tryLabel = new JLabel("Number of Tries: " + tries);
        resetLabel.setHorizontalAlignment(SwingConstants.CENTER);
        tryLabel.setHorizontalAlignment(SwingConstants.CENTER);
        panelBawah.add(resetLabel);
        panelBawah.add(tryLabel);
        resetButton.addActionListener(actionPerformed -> resetGame(cards));
        quitButton.addActionListener(actionPerformed -> exitGame());
        frameUtama.add(panelAtas, BorderLayout.CENTER);
        frameUtama.add(panelBawah, BorderLayout.SOUTH);
        frameUtama.setPreferredSize(new Dimension(750, 750));
        frameUtama.setLocation(250, 150);
        frameUtama.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frameUtama.pack();
        frameUtama.setVisible(true);
        frameUtama.setResizable(false);
        Timer waitTime = new Timer(2000, actionPerformed -> tampilkanSemuaKartu());
        waitTime.setRepeats(false);
        waitTime.start();

    }

    private void exitGame() { System.exit(0);}

    private void resetGame(ArrayList<JavariCards> cards) {
        frameUtama.remove(panelAtas);
        Collections.shuffle(cards);
        panelAtas = new JPanel(new GridLayout(6, 6));
        for (JavariCards card : cards) {
            card.setMatched(false);
            card.release();
            card.getButton().setEnabled(true);
            panelAtas.add(card.getButton());
        }
        frameUtama.add(panelAtas, BorderLayout.CENTER);
        reset++;
        tries = 0;
        resetLabel.setText("Number of Reset: " + reset);
        tryLabel.setText("Number of Tries: " + tries);
    }

    public void tampilkanSemuaKartu() {
        for (JavariCards card : cards) {
            card.press();
        }
        openTime = new Timer(2500, actionPerformed -> sembunyikanKartu());
        openTime.setRepeats(false);
        openTime.start();
    }

    public void sembunyikanKartu() {
        for (JavariCards card : cards) {
            card.release();
        }
    }

    public void balikKartu(JavariCards selectedCard) {
        if ( kartuPertama == null && kartuKedua == null) {
            kartuPertama = selectedCard;
            kartuPertama.press();
        }

        if (kartuPertama != null && kartuPertama != selectedCard && kartuKedua == null) {
            kartuKedua = selectedCard;
            kartuKedua.press();
            timer.start();
        }
    }

    public void checkCards() {
        if (kartuPertama.getID() == kartuKedua.getID()) {
            matchButton();
            if (isGameWon()) {
                Object[] choices = { " Quit", " Play Again!"};
                int option = JOptionPane.showOptionDialog(null,
                        "Congratulations! You Win!",
                        "Dialog Box", JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.PLAIN_MESSAGE, null, choices, choices[1]);
                if (option == 1) {
                    resetGame(cards);
                }
                else {
                    exitGame();
                }
            }
        }
        else {
            kartuPertama.release();
            kartuKedua.release();
            tries++;
            tryLabel.setText("Number of Tries: " + tries);
        }
        kartuPertama = null;
        kartuKedua = null;
    }

    public void matchButton() {
        kartuPertama.getButton().setEnabled(false);
        kartuKedua.getButton().setEnabled(false);
        kartuPertama.setMatched(true);
        kartuKedua.setMatched(true);
    }

    public boolean isGameWon() {
        for (JavariCards card: cards) {
            if (!card.getMatched()) {
                return false;
            }
        }
        return true;
    }

    public static ImageIcon resize(ImageIcon imageIcon) {
        Image image = imageIcon.getImage();
        Image resizedImage = image.getScaledInstance(95, 95, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }
}