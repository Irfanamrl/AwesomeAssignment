import java.util.Random;

public class Cat extends Animal {

    public Cat(String nama, int panjang) {
        super(nama, panjang);
        this.wild = false;
        this.spesies = "cat";
    }

    public void brushed() {
        System.out.println("Time to clean "+ nama + "\'s fur");
        System.out.println(nama + " makes a voice: Nyaaaan...");
        System.out.println("Back to the office!");
    }

    public void cuddled() {
        String output = "";
        String[] kosakata = new String[]{"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
        Random rand = new Random();
        int value = rand.nextInt(4);
        output = kosakata[value];
        System.out.println(nama + " makes a voice: "+ output);
    }

    public void activity(int n) {
        if (n == 1) {
            brushed();
        }
        else if (n == 2) {
            cuddled();
        }
        else {
            System.out.println("You do nothing");
        }
    }

    
}