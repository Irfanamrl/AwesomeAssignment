public class ListOfLevels {
    ListOfCages level1;
    ListOfCages level2;
    ListOfCages level3;

    public ListOfLevels(ListOfCages lv1, ListOfCages lv2, ListOfCages lv3) {
        this.level3 = lv3;
        this.level2 = lv2;
        this.level1 = lv1;
    }

    public ListOfCages getLevel1() {
        return level1;
    }

    public String getLocation() {
        return level1.getCage()[0].getType();
    }

    public void printArrangement() {
        level3.setLevel(3);
        level2.setLevel(2);
        level1.setLevel(1);

        if (!((level1.getCage() == null) && (level2.getCage() == null) && (level3.getCage() == null))) {
            level3.printCages(level3.getCage());
            level2.printCages(level2.getCage());
            level1.printCages(level1.getCage());
        }
    }

    public void afterArrangement() {
        level3.setLevel(1);
        level2.setLevel(3);
        level1.setLevel(2);

        if (!((level1.getCage() == null) && (level2.getCage() == null) && (level3.getCage() == null))) {
            level2.printCages(level2.getReversedCage());
            level1.printCages(level1.getReversedCage());
            level3.printCages(level3.getReversedCage());
        }
    }
}
