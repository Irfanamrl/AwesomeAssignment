import java.util.ArrayList;
import java.util.List;

public class ListOfCages {
    Cage[] reversedCages;
    Cage[] cage;
    int level;

    public ListOfCages(int level, Cage[] paramCages) {
        this.level = level;
        if ((paramCages != null) && (paramCages.length > 0)) {
            this.reversedCages = reverseOrder(paramCages);
        }
        else {
            this.reversedCages = null;
        }
        this.cage = paramCages;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Cage[] getReversedCage() {
        return reversedCages;
    }

    public Cage[] getCage() {
        return cage;
    }

    public void printCages(Cage[] cages) {
        System.out.printf("Level %s: ", this.level);

        if((cages != null) && (cages.length != 0)) {
            if (cages != null) {
                int i = 0; // index of cages
                System.out.printf("%s(%s - %s)", cages[i].getAnimal().getNama(), cages[i].getAnimal().getPanjang()
                        , cages[i].getSizeType());
                while (i < cages.length - 1) {
                    i++;
                    System.out.printf(", %s(%s - %s)", cages[i].getAnimal().getNama(), cages[i].getAnimal().getPanjang()
                            , cages[i].getSizeType());
                }
                System.out.println();
            }
        }
    }

    public static Cage[] reverseOrder(Cage[] cages) {
        Cage[] reverseOrder = new Cage[cages.length];
        for (int i = cages.length - 1; i >= 0; i--) {
            reverseOrder[(cages.length - 1) - i] = cages[i];
        }
        return reverseOrder;
    }
}