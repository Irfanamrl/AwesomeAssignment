public class Cage {
    String type; //1. indoor 2. outdoor
    String sizeType; // A,B,C
    Animal animal;

    public Cage(Animal animal) {
        this.animal = animal;
        if (animal.isWild()) {
            this.type = "outdoor";
        }
        else {
            this.type = "indoor";
        }

        int len = animal.getPanjang();
        if ((len > 90 && animal.isWild()) || (len > 60 && !(animal.isWild()))) {
            this.sizeType = "C";
        }
        else if ((len < 75 && animal.isWild()) || (len < 45 && !(animal.isWild()))) {
            this.sizeType = "A";
        }
        else {
            this.sizeType = "B";
        }
    }

    public String getType() {
        return this.type;
    }

    public String getSizeType() {
        return this.sizeType;
    }

    public Animal getAnimal() {
        return this.animal;
    }
}