public class Lion extends Animal {
    
    public Lion(String nama, int panjang) {
        super(nama, panjang);
        this.wild = true;
        this.spesies = "lion";
    }

    public void hunt() {
        System.out.println("Lion is hunting..");
        System.out.println(nama+ " makes a voice: err...!");
        System.out.println("Back to the office!");
    }

    public void brush() {
        System.out.println("Clean the lions's mane..");
        System.out.println(nama + " makes a voice: Hauhhmm!");
        System.out.println("Back to the office!");
    }

    public void disturb() {
        System.out.println(nama + " makes a voice: HAUHHMM!");
        System.out.println("Back to the office!");
    }

    public void activity(int n) {
        if (n == 1) {
            hunt();
        }
        else if (n == 2) {
            brush();
        }
        else if (n == 3) {
            disturb();
        }
        else {
            System.out.println("You do nothing..");
        }
    }
}