package javari.animal;

public class Mammals extends Animal{
    private boolean hamil = false;

    public Mammals(Integer id, String type, String name, Gender gender, double length,
    double weight, String hamil, Condition condition){
        super(id,type,name,gender,length,weight,condition);
        if(hamil.equals("pregnant")) {
            this.hamil = true;
        }
    }

    public boolean specificCondition(){
        if(getType().equals("Lion")) {
            if(getGender() == Gender.MALE && !hamil) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            if(!hamil) {
                return true;
            }
            else {
                return false;
            }
        }
    }
}