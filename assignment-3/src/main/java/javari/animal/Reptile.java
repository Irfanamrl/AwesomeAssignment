package javari.animal;

public class Reptile extends Animal {
    private boolean jinak = false;;

    public Reptile(Integer id, String type, String name, Gender gender, double length,
    double weight, String jinak, Condition condition) {
        super(id,type,name,gender,length,weight,condition);
        if(jinak.equals("tame")) {
            this.jinak = true;
        }
    }

    public boolean specificCondition() {
        if(jinak) {
            return true;
        } 
        else {
            return false;
        }
    }
}