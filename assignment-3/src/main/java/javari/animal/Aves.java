package javari.animal;

public class Aves extends Animal {
    private boolean bertelur = false;

    public Aves(Integer id, String type, String name, Gender gender, double length, 
    double weight, String bertelur, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if(bertelur.equals("laying eggs")) {
            this.bertelur = true;
        }
    }

    public boolean specificCondition() {
        if(!bertelur) {
            return true;
        }
        else {
            return false;
        }
    }
}